* [Basics](#basics)
* [Catalog Structure](#catalog-structure)
  * [Possible Settings](#possible-settings) 
* [Catalog Management API](#catalog-management-api)
* [Index Management API](#index-management-api)
* [ElasticSearch Access](#elasticsearch-access)
* [Versioning](#versioning)

## Basics

Queequeg serves as a general tool for automatically adding documents to an ElasticSearch instance. You provide a set of URLs and maybe some configurations, then Queequeg will crawl those URLs and add the results to ElasticSearch. For simplicity, we're calling the set of URLs and configuration a "session". When you post and add or delete session, a new session will be queued up and ran later. The sessions will be run in the order they are received; that's a gurantee. However, the URLs listed in a given session may be crawled in any order. 


## Session Body Structure

The body structure of a session is rather flexible and depends, in part, on how you use it to configure the ways your content is indexed. All catalog API endpoints accept this structure. 

```
# no settings, short form
{
    "type1": [...URLs...],
    "type2": [...URLs...]
}
```
```
# global selectors, short form
{
  "selectors": { ...selectors... },
  "types": {
    "type1": [...URLs...],
    "type2": [...URLs...]
  }
}
```
```
# special selectors for type, short form
{
  "selectors": { ...selectors... },
  "types": {
    "type1": {
      "selectors": { ...different selectors... },
      "urls": [...URLs...],
    },
    "type2": [...URLs...]
  }
}
```
```
# special selectors for specific URL, long form
{
  "selectors": { ...selectors... },
  "types": {
    "type1": {
      "selectors": { ...different selectors... },
      "urls": [
        {
          "selectors": { ...even more different selectors... },
          "url": ...
        }, 
        ...
      ],
    },
    "type2": [...URLs...]
  }
}
```

Basically, you can specify any settings at any level. And by "settings" I mean that some keys are signficant and the significant keys are available at all levels: 


* `urls`
* `url`
* `types`

In the absence of any one of these keys, we make an assumption about what's being specified. Beyond just those there are other significant keys. As you see above, `selectors` has significance for how setting the DOM selectors used in extracting properties from HTML. There are other signficant keys: 

* `selectors` - For overriding default selectors
* `properties` - For specifying default properties
* `id`, `error` - These are used internally. **YOU CANNOT USE THEM**.

This:

```
{
  breed: 'hound',
  colors: ['red', 'green', 'brown'],
  types: {
    working: ['hound', 'collie'],
  },
},
```

gets normalized to:

```
{
  working: [
    {
      url: 'hound', 
      id: hash('hound'), 
      properties: {
        breed: 'hound', 
        colors: ['red', 'green', 'brown']
      }
    },
    {
      url: 'collie', 
      id: hash('collie'), 
      properties: {
        breed: 'hound', 
        colors: ['red', 'green', 'brown']
      }
    },
  ],
}
```

### Possible Settings:

#### `selectors`

The default selectors are: 

key | default selector | description
--- | --- | ---
field | `[data-qq]` | The selector to find tags defining the document's properties.
fieldAttr | `data-qq` | The attribute that provides the field name.
fieldValueAttr | `data-qq-value` | The attribute that provides the field's value. If omitted, the text of the tag is used instead.
processAttr | `data-qq-process` | The attribute that specifies any processing actions. The current option is just `"raw"`. Note that all values are trimmed, even if you specify `raw`. 
content | `[data-qq-content]` | The full-text content of your document. No matter the selector, the value from it is stored as the property: "content". If more than one element matches this selector, the values are appended with a newline in the order they are found. 
ignore | `[data-qq-ignore]` | Removed from the content of your document.
tags | `['h1','h2','h3','h4','h5','h6','title']` | These tags are stored on the document under the `tags` field. You can use these to add weights to certain tags when searching.

To specify multiples or nested attributes, you can follow the standard for html input names:

```
<li data-qq="categories[]">fruit</li>
<li data-qq="categories[]">food</li>
<li data-qq="house[parts][door]">A door name.</li>
```

For further examples, look at [test/api/sample.html](../test/api/sample.html).


## Session Management API

When posting a new session, you are not triggering Queequeg's crawler yet. This just adds the session to a queue to be crawled later. These endpoints accept the flexible body structure described above.

#### Add or Update some URLs

```
POST /session/add/butters
{
  "pages": [ 
    "http://www.moca.org/support/membership",
    "http://www.moca.org/support/individuals",
    "http://www.moca.org/support/corporations",
  ],
  "programs": [
    "http://www.moca.org/program/hal-foster-what-is-contemporary",
    "http://www.moca.org/program/members-opening-magdalena-fernandez",
  ]
}
```

### Delete some URLs

```
POST /session/delete/butters 
{
  "pages": [
    "http://www.moca.org/support/individuals"
  ],
  "programs": [
    "http://www.moca.org/program/members-opening-magdalena-fernandez"
  ]
}
```


## Index Management API


### Index or re-index the current catalog

```
POST /run/butters
```

Attempt to run the first session in the queue. If there are no sessions or a session is already started, nothing will happen. It's busy. 


### Get status

Use this to get the status of the currently running session. If we get socket.io support going, the responses here will be the same. 

```
GET /status/butters

# sample response 
{
  "status": "in-progress",
  "totalDocuments": 1522,
  "indexedCount": 425,
  "pendingCount": 1097,
  "errors": [
    {
      "serverResponse": 404,
      "url": "http://moca.org/programs/elephant"
    }
  ]
}
```

## ElasticSearch Access

Queequeg only provides this extra layer of auto-indexing for a catalog of URLs. Everything else is just ElasticSearch. For searching and other needs, you may directly send requests to ElasticSearch.

```
/search
```

For example, to search the butters index:

```
GET /search/butters/_search
{
  "query": {
    "match": {
      "body": "rock climbing"
    }
  }
}
```

## Versioning

Right now, there's just `0.1.0` but should you need to specify a version in the future you can pass in a header: 

```
Accept-Version: ^1.0.0
```

Works like NPM or any other semver setup.