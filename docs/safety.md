## Safe Queequeg Indexing 

Because requests to index can come frequently enough to overlap and cause race conditions, we need a way of isolating the indexing process and preventing it from working with stale or modified data. 


### Changes

1. The major change is that catalog API calls are all now immutable and create new "sessions" of data to be indexed. 


### Implementation

**First Request**

Setting the catalog will create a new catalog version with it's related alias pointing to the new version. It will add the documents to the catalog. Then it will create a session for that catalog to be indexed. Then, next time around, we'll just create a new version of the catalog and index, then create a new session representing those changes as well. 

`POST /catalog/butters` (`PUT` should work as well)

1. Create a new `qq_butters` version, `qq_butters_v1` (and update alias). 
1. Add all documents to `qq_butters_v1`. 
1. Create a new document under `qq_butters_stats/sessions/current` document:
    
    ```javascript
    {
      // can be "ready', "paused", "started", "completed"
      "status": "ready",
      
      // or "SOME" or "DELETE", this specifies how the catalog is to be applied 
      "target": "ALL",
      
      "timestamp": tstamp,
      
      // these combos will be unique per session and will prevent any overlap
      "catalogAlias": "qq_butters",
      "catalogIndex": "qq_butters_v1",
      "contentAlias": "butters",
      "contentIndex": "butters_v1", // same version as catalog (doesn't have to exist yet)
      
      // counts calculated after
      "total": 3939,
      "completed": 292,
      "errors": 39
    }
    ```
    
**First run**

`POST /run/butters` (`PUT` should work as well)

1. Get oldest session that is "ready" (OR "paused"). 
1. [Update][1] session status to "running". 
1. Create the `butters_v1` index.
1. Mark catalog documents as started: `qq_status: "started"`. 
1. Then index like normal from `catalogIndex` to `contentIndex` in batches of 100.
  * When a document is completed, update catalog document status: `qq_status: "completed"`
  * When there's a document error, update catalog document: `qq_status: "error", error: ...`.
  * When done, if not all documents are accounted for, set `qq_status: "started"` to `qq_status: "error"`.
1. Update counts and session status to "paused" (or "completed" if completed). It's completed when all documents are either `qq_status: "completed"` or `qq_status: "error"`. 
1. If completed, we can now update the real alias: `butters -> butters_v1`. 

**Next request**

`POST /catalog/butters` 

Same as last time, but we increment the index versions. 

**Next run**

Same as last time.


[1]: https://www.elastic.co/guide/en/elasticsearch/reference/1.7/docs-update.html
