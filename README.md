# Queequeg

Search's Favorite Bedfellow

![gillian_and_queequeg](docs/images/queequeg.png)

### Running in Development

```
git clone git@github.com:castiron/queequeg.git
cd queequeg
cp .env.sample .env
npm install
knex migrate:latest
npm start
```

### Using Docker

You can optionally run ElasticSearch in a docker container (so you're stuff doesn't mess with the staging content). To do that install docker and then run: 

```
docker run -p 39200:9200 --name=qqsearch --rm elasticsearch:2
```

Then you need to update your `.env` file to suit. (On OSX, to get your docker VM IP, you do something like: `docker-machine ip default`).

```
ELASTICSEARCH_HOST=192.168.99.100
ELASTICSEARCH_PORT=39200
```


#### The foreman option

If you have `foreman` installed, you can just run `foreman start` to use the Procfile for starting the app and starting the docker ElasticSearch container. Note that the Procfile allows CORS for all origins. You probably don't want to use this on production.

### [Checkout the API](docs/API.md)

### Source Structure

#### Servers

There are two servers in [/src/serving](src/serving): dev & prod. They both do the same thing. But they just integrate with their environments differently. While all the setup and routes are here, the actual work is in the [api](src/api).

#### API

The API is mainly split into two groups: catalog management and index management. Catalogs are special ElasticSearch indices. It holds extra information about _what's going to be indexed_. There is also the search proxy which just passes requests on to ElasticSearch.


#### Migrations

Queequeg uses sqlite (w/[knex](http://knexjs.org/)) to keep the queue. You'll need to run migrations before starting:

```
knex migrate:latest
```

### ElasticSearch version

We need 2.x. Not higher or lower right now. We're using the `_timestamp` mapping which will change in 3.x and has changed since 1.x. So, let's keep it at 2.x for now. 


