import 'must';
import SessionManager from '../../src/api/session/manager';
import knex, {clearAllTables} from '../../src/database/connection';
import {assertDoesntExist, fails} from '../assert';


describe('Session', () => {
  beforeEach(clearAllTables);

  it('adds a session', () => {
    return SessionManager.addAll('meat', {
      red: ['a', 'b', 'c'],
      white: ['d', 'e', 'f'],
    })
      .then(() => knex('sessions').count('id as count'))
      .then((rows) => rows[0].count.must.eql(1))
      .then(() => knex('instructions').count('id as count'))
      .then((rows) => rows[0].count.must.eql(6));
  });

  it('adds more sessions', () => {
    return SessionManager.addAll('fish', {
      fresh: ['salmon', 'carp'],
    })
      .then(() => SessionManager.addAll('fish', {fresh: ['catfish']}))
      .then(() => knex('sessions').count('id as count').first('count'))
      .then((row) => row.count.must.eql(2))
      .then(() => knex('instructions').count('id as count').first('count'))
      .then((row) => row.count.must.eql(3));
  });

  it('gets the oldest session', () => {
    return SessionManager.addAll('vehicle', {cars: ['rabbit', 'golf']})
      .then(() => SessionManager.addAll('vehicle', {cars: ['bug']}))
      .then(() => SessionManager.addAll('vehicle', {trucks: ['bronco']}))
      .then(() => SessionManager.checkoutOldest('vehicle'))
      .then((oldest) => {
        oldest.status.must.eql('started');
      });
  });

  it('gets nothing when there are no sessions', () => {
    return SessionManager.checkoutOldest('vehicle')
      .then(assertDoesntExist);
  });

  it('gets nothing when at least one session is already started', () => {
    return SessionManager.addAll('vehicle', {cars: ['rabbit', 'golf']})
      .then(() => SessionManager.addAll('vehicle', {cars: ['bug']}))
      .then(() => SessionManager.addAll('vehicle', {trucks: ['bronco']}))
      .then(() => SessionManager.checkoutOldest('vehicle'))
      .then(() => SessionManager.checkoutOldest('vehicle'))
      .then(assertDoesntExist);
  });

  it('only marks one session as started when checking out more than one at a time', () => {
    return SessionManager.addAll('vehicle', {cars: ['rabbit', 'golf']})
      .then(() => SessionManager.addAll('vehicle', {cars: ['bug']}))
      .then(() => SessionManager.addAll('vehicle', {trucks: ['bronco']}))
      .then(() => SessionManager.addAll('vehicle', {boats: ['canoe']}))
      .then(() => SessionManager.addAll('vehicle', {planes: ['747']}))
      .then(() => SessionManager.addAll('vehicle', {balloons: ['zeppelin', 'blimp']}))
      .then(() => Promise.all([
        SessionManager.checkoutOldest('vehicle'),
        SessionManager.checkoutOldest('vehicle'),
        SessionManager.checkoutOldest('vehicle'),
        SessionManager.checkoutOldest('vehicle'),
        SessionManager.checkoutOldest('vehicle'),
        SessionManager.checkoutOldest('vehicle'),
        SessionManager.checkoutOldest('vehicle'),
        SessionManager.checkoutOldest('vehicle'),
      ]))
      .then(() => {
        return knex('sessions')
          .where('status', 'started')
          .count('id as count')
          .first('count')
          .then((row) => row.count.must.eql(1));
      });
  });

  it('only checks out the relevant session by index', () => {
    return SessionManager.addAll('food', {bread: ['rye', 'sourdough']})
      .then(() => SessionManager.addAll('vehicle', {trucks: ['pickup', 'ford']}))
      .then(() => SessionManager.addAll('food', {cake: ['pound', 'angel']}))
      .then(() => SessionManager.checkoutOldest('vehicle'))
      .then((session) => session.index.must.eql('vehicle'));
  });

  it('interrupts sessions', () => {
    return SessionManager.addAll('vehicle', {planes: [747]})
      .then((sessionId) => {
        return SessionManager.interruptSession(sessionId, 'paused')
          .then(() => knex('sessions').where({id: sessionId}).first('*'))
          .then((row) => {
            row.status.must.eql('pending');
            row.interrupt.must.eql('paused');
          });
      });
  });

  it('applies interrupts', () => {
    return SessionManager.addAll('vehicle', {planes: [747]})
      .then((sessionId) => {
        return SessionManager.interruptSession(sessionId, 'finished')
          .then(() => SessionManager.applyInterrupts())
          .then(() => knex('sessions').where({id: sessionId}).first('*'))
          .then((row) => {
            row.status.must.eql('finished');
            if (row.interrupt) fails();
          });
      });
  });
});
