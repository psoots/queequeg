import 'must';
import {normalizeForType, normalizeTypes} from '../../src/api/session/normalizer';
import {hash} from '../../src/tools';


describe('Normalizer', () => {
  function assertNormalizedFruit(toNormalize, goal) {
    normalizeForType(toNormalize, 'fruit').must.eql(goal);
  }

  function assertNormalizedDogs(toNormalize, goal) {
    normalizeTypes(toNormalize).must.eql(goal);
  }

  it('normalizes for types from array of objects', () => {
    assertNormalizedFruit(
      [
        {url: 'url1', selectors: {blah: 'apple'}},
        {url: 'url2', selectors: {blah: 'banana'}},
        {url: 'url3', selectors: {blah: 'peach'}},
      ],
      {
        fruit: [
          {url: 'url1', selectors: {blah: 'apple'}, id: hash('url1')},
          {url: 'url2', selectors: {blah: 'banana'}, id: hash('url2')},
          {url: 'url3', selectors: {blah: 'peach'}, id: hash('url3')},
        ],
      });
  });

  it('normalizes for types from array of strings', () => {
    assertNormalizedFruit(
      [
        'url1',
        'url2',
        'url3',
      ],
      {
        fruit: [
          {url: 'url1', id: hash('url1')},
          {url: 'url2', id: hash('url2')},
          {url: 'url3', id: hash('url3')},
        ],
      }
    );
  });

  it('normalizes for types from object with URLs as array of objects', () => {
    assertNormalizedFruit(
      {
        urls: [
          {url: 'url1', selectors: {blah: 'apple'}},
          {url: 'url2', selectors: {blah: 'banana'}},
          {url: 'url3', selectors: {blah: 'peach'}},
        ],
      },
      {
        fruit: [
          {url: 'url1', selectors: {blah: 'apple'}, id: hash('url1')},
          {url: 'url2', selectors: {blah: 'banana'}, id: hash('url2')},
          {url: 'url3', selectors: {blah: 'peach'}, id: hash('url3')},
        ],
      }
    );
  });

  it('normalizes for types from object with URLs as array of strings', () => {
    assertNormalizedFruit(
      {
        urls: [
          'url1',
          'url2',
          'url3',
        ],
      },
      {
        fruit: [
          {url: 'url1', id: hash('url1')},
          {url: 'url2', id: hash('url2')},
          {url: 'url3', id: hash('url3')},
        ],
      }
    );
  });


  it('normalizes with defaults set at base level', () => {
    assertNormalizedFruit(
      {
        selectors: {
          blah: 'default',
        },
        urls: [
          'url1',
          {url: 'url2', selectors: {blah: 'special'}},
          'url3',
        ],
      },
      {
        fruit: [
          {url: 'url1', selectors: {blah: 'default'}, id: hash('url1')},
          {url: 'url2', selectors: {blah: 'special'}, id: hash('url2')},
          {url: 'url3', selectors: {blah: 'default'}, id: hash('url3')},
        ],
      }
    );
  });


  it('normalizes for objects', () => {
    assertNormalizedDogs(
      {
        working: [
          'hound',
          'collie',
        ],
        toy: [
          'poodle',
        ],
      },
      {
        working: [
          {url: 'hound', id: hash('hound')},
          {url: 'collie', id: hash('collie')},
        ],
        toy: [
          {url: 'poodle', id: hash('poodle')},
        ],
      }
    );
  });

  it('normalizes for objects with settings', () => {
    assertNormalizedDogs(
      {
        selectors: {
          blah: 'default',
        },
        types: {
          working: [
            'hound',
            'collie',
          ],
          toy: {
            selectors: {
              bluh: 'toy-default',
            },
            urls: [
              {url: 'poodle', selectors: {bluh: 'special1', blah: 'special2'}},
              'terrier',
            ],
          },
        },
      },
      {
        working: [
          {url: 'hound', selectors: {blah: 'default'}, id: hash('hound')},
          {url: 'collie', selectors: {blah: 'default'}, id: hash('collie')},
        ],
        toy: [
          {url: 'poodle', selectors: {bluh: 'special1', blah: 'special2'}, id: hash('poodle')},
          {url: 'terrier', selectors: {bluh: 'toy-default', blah: 'default'}, id: hash('terrier')},
        ],
      }
    );
  });

  it('converts non-special keys into properties', () => {
    assertNormalizedDogs(
      {
        breed: 'hound',
        colors: ['red', 'green', 'brown'],
        types: {
          working: ['hound', 'collie'],
        },
      },
      {
        working: [
          {url: 'hound', id: hash('hound'), properties: {breed: 'hound', colors: ['red', 'green', 'brown']}},
          {url: 'collie', id: hash('collie'), properties: {breed: 'hound', colors: ['red', 'green', 'brown']}},
        ],
      }
    );
  });

  it('only allows unique urls');
});
