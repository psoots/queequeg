import 'must';
import extract from '../../src/api/index/extract';
import sampleFile from '../data/sample';
import {fails} from '../assert';

describe('Extract', () => {
  const extracted = extract(sampleFile());

  it('extracts fields', () => {
    extracted.fern.must.eql('lady');
  });

  it('extracts fields with specified values', () => {
    extracted.brownie.must.eql('scout');
  });

  it('extracts text values without hmlt', () => {
    extracted.shirt.must.eql('grass');
  });

  it('extracts arrays of values', () => {
    extracted.tree.must.eql(['pine', 'fir', 'leafy', 'dead']);
  });

  it('extracts nested objects', () => {
    extracted.furniture.must.eql({
      chairs: ['stool', 'lawn'],
      tables: ['desk', 'kiosk'],
    });
  });

  it('extracts content', () => {
    extracted.content.must.contain('### found_marker ###');
  });

  it('extracts without ignored content', () => {
    extracted.content.must.not.contain('### ignored_marker ###');
  });

  it('extracts fields, even within content', () => {
    extracted.shutter.must.eql('closed');
  });

  it('extracts tags', () => {
    extracted.tags.h3.must.include('beta');
  });

  it('extracts multiple tags', () => {
    extracted.tags.h1.must.eql(['alpha', 'other']);
  });

  it('extracts with processing: raw', () => {
    extracted.process.one.must.eql(`<div class="humbug">\n\t\t\tsome real stuff here\n\t\t</div>`);
  });

  it('extracts trimmed with no processing', () => {
    extracted.process.two.must.eql('some real stuff here');
  });

  it('handles empties', () => {
    if (extracted.empty && extracted.empty.one) {
      fails();
    }
  });

  it('handles empties using attribute', () => {
    if (extracted.empty && extracted.empty.two) {
      fails();
    }
  });

  it('handles empties using attribute but has text', () => {
    if (extracted.empty && extracted.empty.three) {
      fails();
    }
  });

  it('handles empties using attribute but has html', () => {
    if (extracted.empty && extracted.empty.four) {
      fails();
    }
  });

  it('strips html', () => {
    extracted.content.must.contain('!!!nohtml!!!');
  });
});
