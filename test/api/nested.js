import 'must';
import applyNestedName from '../../src/api/index/nested';


describe('Apply nested names', () => {
  it('applies nested names', () => {
    const res = applyNestedName({}, 'brown[fish][swim]', 333);
    res.must.eql({brown: {fish: {swim: 333}}});
  });

  it('applies non-nested names', () => {
    const res = applyNestedName({}, 'brown', 333);
    res.must.eql({brown: 333});
  });

  it('applies nested names, multiple times', () => {
    const obj = applyNestedName({}, 'brown[fish][swim]', 333);
    const res = applyNestedName(obj, 'brown[fish][float]', 334);
    res.must.eql({brown: {fish: {swim: 333, float: 334}}});
  });

  it('applies array items', () => {
    const res = applyNestedName({}, 'birds[]', 'Canary');
    res.must.eql({birds: ['Canary']});
  });

  it('applies array items, multiple times', () => {
    const obj = applyNestedName({}, 'birds[]', 'Canary');
    const res = applyNestedName(obj, 'birds[]', 'Wren');
    res.must.eql({birds: ['Canary', 'Wren']});
  });

  it('applies nested array items', () => {
    const res = applyNestedName({}, 'birds[breeds][]', 'Canary');
    res.must.eql({birds: {breeds: ['Canary']}});
  });

  it('applies nested array items, multiple times', () => {
    const obj = applyNestedName({}, 'birds[breeds][]', 'Canary');
    const res = applyNestedName(obj, 'birds[breeds][]', 'Wren');
    res.must.eql({birds: {breeds: ['Canary', 'Wren']}});
  });

  it('applies nested array items between object items', () => {
    const res = applyNestedName({}, 'birds[][start][fish][]', 393);
    res.must.eql({birds: [{start: {fish: [393]}}]});
  });

  // see how this is different?
  it('applies nested array items between object items, multiple times', () => {
    const obj = applyNestedName({}, 'birds[][start][fish][]', 393);
    const res = applyNestedName(obj, 'birds[][start][fish][]', 396);
    res.must.eql({
      birds: [
        {start: {fish: [393]}},
        {start: {fish: [396]}},
      ],
    });
  });
});
