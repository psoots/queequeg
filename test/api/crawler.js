import 'must';
import TestServer from '../data/server';
import map from 'mout/array/map';
import size from 'mout/object/size';
import range from 'mout/array/range';
import crawl from '../../src/api/index/crawler';

let server;
const arr100 = map(range(1, 100), (x) => `test/load-${x}`);
const arr50 = map(range(1, 50), (x) => `test/ok-${x}`);


function unTestUrl(url) {
  return url.replace(new RegExp('^' + TestServer.url() + 'test\/'), '');
}

function testUrls(paths) {
  return map(paths, TestServer.url);
}

function crawlAll(paths, useTestServer = true) {
  return new Promise((ready) => {
    const results = {};
    const errors = {};
    const urls = useTestServer ? testUrls(paths) : paths;

    const setter = (url, body) => {
      results[unTestUrl(url)] = body;
    };
    const failHandler = (url, errorMsg) => {
      errors[unTestUrl(url)] = errorMsg;
    };

    crawl(urls, setter, failHandler)
      .then(() => ready({results, errors}));
  });
}

describe('Crawler', () => {
  before(() => {
    server = new TestServer();
    server.routeGetHtml('/test/works', () => 'result1');
    arr100.forEach((x) => server.routeGetHtml(x, () => 'same-for-all'));
    arr50.forEach((x) => server.routeGetHtml(x, () => 'same-for-all'));
    server.routeGetHtml('/test/resource.html', () => 'some-resource');
    server.routeGetHtml('/test/linked', () => {
      const resourceUrl = TestServer.url('/test/resource.html');
      return `<!doctype html><html><body><a href="${resourceUrl}">some resource</a></body></html>`;
    });
    return server.listen();
  });

  after(() => {
    server.close();
  });

  it('works!', () => {
    return crawlAll(['test/works'])
      .then((obj) => {
        obj.results.works.must.eql('result1');
      });
  });

  it('crawls a lot', function() {
    // must use _real_ function
    this.timeout(0);

    return crawlAll(arr100)
      .then((obj) => {
        size(obj.results).must.eql(100);
      });
  });

  it('does not fetch linked resources', () => {
    return crawlAll(['test/linked'])
      .then((obj) => {
        size(obj.results).must.eql(1);
      });
  });

  it("one bad appled doesn't ruin the whole batch", function() {
    // must use _real_ function
    this.timeout(0);

    const urls = arr50.slice();
    urls[33] = 'test/bad-34';
    return crawlAll(urls)
      .then((obj) => {
        size(obj.results).must.eql(49);
        size(obj.errors).must.eql(1);
      });
  });
});
