import ops from '../src/api/search/operations';
import map from 'mout/array/map';
import choice from 'mout/random/choice';
import isArray from 'mout/lang/isArray';
import merge from 'mout/object/merge';
import SessionManager from '../src/api/session/manager';
import IndexManager from '../src/api/index/manager';
import {assertUrlExists, assertUrlDoesntExist, succeeds, fails} from './assert';
import sampleFile from './data/sample';
import TestServer from './data/server';
import knex, {clearAllTables} from '../src/database/connection';

let server;
const validURLs = [
  'car', 'bike', 'truck', 'sedan',
  'monopoly', 'sorry', 'cranium', 'parchisi',
  'dog', 'cat',
  'cheese', 'apple', 'candy',
  'cards?hang=1',
];

function toUrl(given) {
  if (isArray(given)) {
    return map(given, toUrl);
  }
  return TestServer.url(`integrate/${given}`);
}

function fromUrl(url) {
  return url.replace(new RegExp('^' + TestServer.url() + 'integrate\/'), '');
}

const dataOne = {
  vehicles: toUrl(['car', 'bike', 'truck']),
  games: toUrl(['monopoly', 'sorry', 'cranium']),
};


const dataTwo = {
  vehicles: toUrl(['car', 'plane', 'sedan']), // plane is invalid
  games: toUrl(['monopoly', 'sorry', 'cranium']),
  pets: toUrl(['dog', 'cat', 'fish']), // fish is invalid
};


const dataThree = {
  games: toUrl(['parchisi']),
  food: toUrl(['cheese', 'apple', 'candy']),
};

const dataFour = {
  games: toUrl(['monopoly']),
};

const dataFive = {
  games: toUrl(['cards?hang=2000']),
};

function hang(dataObj, duration = 1000) {
  const obj = merge({}, dataObj);
  const key = choice(Object.keys(obj));
  const val = obj[key].pop();
  obj[key].push(`${val}?hang=${duration}`);
  return obj;
}

function reset() {
  if (server) {
    server.close();
  }
  server = new TestServer();
  const html = sampleFile();
  validURLs.forEach((url) => {
    server.routeGetHtml(`integrate/${url}`, () => html);
  });
  server.listen();

  return Promise.all([
    clearAllTables(),
    ops.deleteIndex('butters'),
  ]);
}

function unset() {
  server.close();
  server = null;
}

describe('Integrated Crawling/Indexing', function() {
  this.timeout(0);

  beforeEach(reset);
  after(unset);

  it('runs an index', () => {
    return assertUrlDoesntExist('butters', 'vehicles', toUrl('car'))
      .then(() => SessionManager.addAll('butters', dataOne))
      .then(() => IndexManager.run('butters'))
      .then(() => Promise.all([
        assertUrlExists('butters', 'vehicles', toUrl('car')),
      ]));
  });

  it('captures errors', () => {
    return assertUrlDoesntExist('butters', 'vehicles', toUrl('car'))
      .then(() => SessionManager.addAll('butters', dataTwo))
      .then(() => IndexManager.run('butters'))
      .then(() => {
        return knex('errors')
          .count('id as count')
          .first('count')
          .then((row) => row.count.must.eql(2));
      });
  });

  it('deletes stuff', () => {
    return assertUrlDoesntExist('butters', 'games', toUrl('monopoly'))
      .then(() => SessionManager.addAll('butters', dataOne))
      .then(() => IndexManager.run('butters'))
      .then(assertUrlExists('butters', 'games', 'monopoly'))
      .then(() => SessionManager.deleteAll('butters', dataFour))
      .then(() => IndexManager.run('butters'))
      .then(assertUrlDoesntExist('butters', 'games', toUrl('monopoly')));
  });

  it('updates session to finished', () => {
    return SessionManager.addAll('butters', dataOne)
      .then(() => IndexManager.run('butters'))
      .then(() => knex('sessions').where('index', 'butters').first('*'))
      .then((session) => session.status.must.eql('finished'));
  });

  it('updates session to paused', () => {
    return SessionManager.addAll('butters', dataOne)
      .then(() => IndexManager.run('butters', 3)) // lower batch size
      .then(() => knex('sessions').where('index', 'butters').first('*'))
      .then((session) => session.status.must.eql('paused'));
  });

  it('runs multiple sessions', () => {
    return SessionManager.addAll('butters', dataOne)
      .then(() => SessionManager.addAll('butters', dataTwo))
      .then(() => SessionManager.addAll('butters', dataThree))
      .then(() => IndexManager.run('butters'))
      .then(() => IndexManager.run('butters'))
      .then(() => IndexManager.run('butters'))
      .then(() => knex('sessions').where('status', 'finished').count('id as count').first('count'))
      .then((row) => row.count.must.eql(3));
  });

  it('runs in batches', () => {
    let firstSession;
    let secondSession;

    // add first set of data and save ID
    return SessionManager.addAll('butters', dataOne)
      .then((sessionId) => Promise.resolve(firstSession = sessionId))

      // add second set of data and save ID
      .then(() => SessionManager.addAll('butters', dataTwo))
      .then((sessionId) => Promise.resolve(secondSession = sessionId))

      // everything starts as "pending"
      .then(() => knex('sessions').where('id', firstSession).first('*'))
      .then((firstSessionRow) => firstSessionRow.status.must.eql('pending'))
      .then(() => knex('sessions').where('id', secondSession).first('*'))
      .then((secondSessionRow) => secondSessionRow.status.must.eql('pending'))

      // after running once, the first session should be paused
      .then(() => IndexManager.run('butters', 2))
      .then(() => knex('sessions').where('id', firstSession).first('*'))
      .then((firstSessionRow) => firstSessionRow.status.must.eql('paused'))
      .then(() => knex('sessions').where('id', secondSession).first('*'))
      .then((secondSessionRow) => secondSessionRow.status.must.eql('pending'))

      // ok finish the paused session
      .then(() => IndexManager.run('butters'))
      .then(() => knex('sessions').where('id', firstSession).first('*'))
      .then((firstSessionRow) => firstSessionRow.status.must.eql('finished'))
      .then(() => knex('sessions').where('id', secondSession).first('*'))
      .then((secondSessionRow) => secondSessionRow.status.must.eql('pending'))

      // by now, both sessions will be finished
      .then(() => IndexManager.run('butters'))
      .then(() => knex('sessions').where('id', firstSession).first('*'))
      .then((firstSessionRow) => firstSessionRow.status.must.eql('finished'))
      .then(() => knex('sessions').where('id', secondSession).first('*'))
      .then((secondSessionRow) => secondSessionRow.status.must.eql('finished'))
      ;
  });

  it('does nothing when there is nothing to do (no sessions or a session is started)', () => {
    return IndexManager.run('butters').then(succeeds, fails);
  });

  it('does nothing when there is nothing to do for any index', () => {
    return IndexManager.runAll().then(succeeds, fails);
  });

  it('runs all indices (once)', () => {
    return Promise.all([
      SessionManager.addAll('butters1', dataOne),
      SessionManager.addAll('butters2', dataTwo),
      SessionManager.addAll('butters2', dataThree),
    ])
      .then(() => IndexManager.runAll())
      .then(() => {
        return knex('sessions')
          .where('status', 'pending')
          .count('id as count')
          .first('count')
          .then((row) => row.count.must.eql(1));
      })
      .then(() => {
        return knex('sessions')
          .where('status', 'finished')
          .count('id as count')
          .first('count')
          .then((row) => row.count.must.eql(2));
      });
  });

  it('only runs one at a time (of the same index)', () => {
    // we're testing this by adding a session that will take a
    // significant amount of time. And while we're busy working on
    // that one, we should not also be able to start any others.

    return SessionManager.addAll('butters', hang(dataOne, 1000))
      .then(() => Promise.all([
        SessionManager.addAll('butters', dataTwo),
        SessionManager.addAll('butters', dataThree),
        SessionManager.addAll('butters', dataFour),

        SessionManager.addAll('butters', dataOne),
        SessionManager.addAll('butters', dataTwo),
        SessionManager.addAll('butters', dataThree),
        SessionManager.addAll('butters', dataFour),

        SessionManager.addAll('butters', dataOne),
        SessionManager.addAll('butters', dataTwo),
        SessionManager.addAll('butters', dataThree),
        SessionManager.addAll('butters', dataFour),
      ]))
      .then(() => Promise.all([
        IndexManager.run('butters'),
        IndexManager.run('butters'),
        IndexManager.run('butters'),
        IndexManager.run('butters'),
        IndexManager.run('butters'),
        IndexManager.run('butters'),
        IndexManager.run('butters'),
        IndexManager.run('butters'),
        IndexManager.run('butters'),
        IndexManager.run('butters'),
      ]))
      .then((res) => {
        res.filter((x) => x === 'finished').length.must.eql(1);
        return knex('sessions')
          .where('status', 'finished')
          .count('id as count')
          .first()
          .then((row) => row.count.must.eql(1));
      });
  });
});

describe('Integrated Status', function() {
  this.timeout(0);

  beforeEach(reset);
  after(unset);

  it('gets "waiting" status', () => {
    return SessionManager.getStatus('butters')
      .then((result) => {
        result.status.must.eql('waiting');
        result.sessions.started.must.be.empty();
        result.sessions.pending.must.be.empty();
        result.sessions.finished.must.be.empty();
      });
  });

  it('gets "pending" status', () => {
    return SessionManager.addAll('butters', dataOne)
      .then(() => SessionManager.getStatus('butters'))
      .then((result) => {
        result.status.must.eql('pending');
        result.sessions.pending.length.must.eql(1);
        result.sessions.started.length.must.eql(0);
        result.sessions.finished.length.must.eql(0);
      });
  });

  it('gets "pending" status with multiple sessions', () => {
    return SessionManager.addAll('butters', dataOne)
      .then(() => SessionManager.addAll('butters', dataTwo))
      .then(() => SessionManager.getStatus('butters'))
      .then((result) => {
        result.status.must.eql('pending');
        result.sessions.pending.length.must.eql(2);
        result.sessions.started.length.must.eql(0);
        result.sessions.finished.length.must.eql(0);
      });
  });

  it('gets "started" status with multiple sessions', () => {
    return SessionManager.addAll('butters', dataFive)
      .then(() => SessionManager.addAll('butters', dataOne))
      .then(() => {
        // intentional race here where the IndexManager will hang on a URL
        // this let's us check the status during the indexing
        const longWait = IndexManager.run('butters');
        const shortWait = SessionManager.getStatus('butters')
          .then((result) => {
            result.status.must.eql('started');
          });

        return Promise.all([longWait, shortWait]);
      });
  });

  it('gets errors for sessions', () => {
    return SessionManager.addAll('butters', dataTwo)
      .then(() => IndexManager.run('butters'))
      .then(() => SessionManager.getStatus('butters'))
      .then((result) => {
        result.status.must.eql('pending');
        result.sessions.finished.length.must.eql(1);
        const sesh = result.sessions.finished[0];
        sesh.errors.length.must.eql(2);
      });
  });
});
