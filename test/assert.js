import 'must';
import {hash} from '../src/tools';
import ops from '../src/api/search/operations';

function makeUrl(index, type, url, doHash = true) {
  let id;
  let path;
  if (doHash) {
    id = hash(url);
    path = `${index}/${type}/hash('${url}')`;
  } else {
    id = url;
    path = `${index}/${type}/${url}`;
  }
  return {id, path};
}

export function assertExists(msg) {
  return (exists) => {
    if (!exists) throw new Error(msg);
  };
}

export function assertDoesntExist(msg) {
  return (exists) => {
    if (exists) throw new Error(msg);
  };
}

export function assertAliasDoesntExist(alias) {
  return ops.aliasExists(alias)
    .then(assertDoesntExist(`Alias ${alias} exists but shouldn't`));
}

export function assertAliasExists(alias) {
  return ops.aliasExists(alias)
    .then(assertExists(`Alias ${alias} does not exist but should`));
}

export function assertIndexDoesntExist(index) {
  return ops.indexExists(index)
    .then(assertDoesntExist(`Index ${index} exists but shouldn't`));
}

export function assertIndexExists(index) {
  return ops.indexExists(index)
    .then(assertExists(`Index ${index} does not exist but should`));
}

// if exclusive, then the alias can _only_ equal the given index
export function assertAliasEqualsIndex(alias, index, exclusive = true) {
  return ops.getAlias(alias)
    .then((info) => {
      const foundIndices = Object.keys(info);
      if (exclusive) {
        foundIndices.must.have.length(1);
        const foundIndex = foundIndices.pop();
        foundIndex.must.eql(index);
        return;
      }
      foundIndices.must.include(index);
    });
}

export function assertAliasDoesntEqualIndex(alias, index) {
  return ops.getAlias(alias)
    .then((info) => {
      const foundIndices = Object.keys(info);
      foundIndices.must.not.include(index);
    });
}


export function assertUrlExists(index, type, url, doHash = true) {
  const {id, path} = makeUrl(index, type, url, doHash);
  return ops.documentExists(index, type, id)
    .then(assertExists(`Document does not exist but should: ${path}`));
}

export function assertUrlDoesntExist(index, type, url, doHash = true) {
  const {id, path} = makeUrl(index, type, url, doHash);
  return ops.documentExists(index, type, id)
    .then(assertDoesntExist(`Document does exist but shouldn't: ${path}`));
}

export function succeeds() { true.must.be.true(); }
export function fails() { true.must.be.false(); }
