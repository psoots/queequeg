import restify from 'restify';


function renderFakeResult(cb) {
  return (req, res, next) => {
    const body = cb(req.params);
    res.writeHead(200, {
      'Content-Length': Buffer.byteLength(body),
      'Content-Type': 'text/html',
    });
    res.write(body);
    res.end();
    next();
  };
}

export default class TestServer {

  constructor() {
    this.data = {};
    this.server = restify.createServer({});
    this.server.use(restify.queryParser());
  }

  listen() {
    return new Promise((ready) => this.server.listen(8181, ready));
  }

  routeGetHtml(path, cb) {
    const func = renderFakeResult(cb);
    this.server.get(path, (req, res, next) => {
      if (req.query.hang) {
        setTimeout(() => func(req, res, next), req.query.hang);
      } else {
        func(req, res, next);
      }
    });
  }

  routeGet404(path) {
    this.server.get(path, (req, res, next) => {
      res.send(404);
      next();
    });
  }

  static url(path = '') {
    return 'http://127.0.0.1:8181/' + path;
  }

  close() {
    return new Promise((ready) => this.server.close(ready));
  }
}
