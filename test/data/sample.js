import fs from 'fs';
import path from 'path';

export default function sampleFile() {
  const filePath = path.resolve(`${__dirname}/sample.html`);
  return fs.readFileSync(filePath, 'utf8');
}
