import knex from '../../database/connection';
import log from '../../log';

export default function migrate() {
  log.info('Running migrations');
  return knex.migrate.latest().spread((batchNo, logs) => {
    if (logs.length === 0) {
      log.info('Already up to date');
    }
    log.info(`Batch ${batchNo} run: ${logs.length} migrations \n` + logs.join('\n'));
  });
}

