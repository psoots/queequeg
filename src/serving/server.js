import restify from 'restify';
import sessionRoutes from './routes/session';
import indexRoutes from './routes/index';
import statusRoutes from './routes/status';
import forgivingJsonBodyParser from './forgivingJsonParser';
import searchProxy from './proxy';

const server = restify.createServer({});

// must come first!
searchProxy(server);

server.use(restify.throttle({
  rate: 5,  // max requests per second
  burst: 5, // max requests per second
  ip: true,
}));
server.use(restify.acceptParser(server.acceptable));
server.use(restify.jsonBodyParser({
  mapParams: false,
}));
server.use(forgivingJsonBodyParser);

sessionRoutes(server);
indexRoutes(server);
statusRoutes(server);

export default server;
