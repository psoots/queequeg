import SessionManager from '../../api/session/manager';
import {respondWithBody, respond, versioned} from '../response';


export default function sessionRoutes(server) {
  const v1 = versioned(['0.1.0']);

  /**
   * Register or re-register a catalog of URLs to be indexed
   */
  server.post(v1('/session/add/:name'),
    respondWithBody(SessionManager.addAll));

  /**
   * Delete whole index or by URLs
   */
  server.post(v1('/session/delete/:name'),
    respondWithBody(SessionManager.deleteAll));

  /**
   * Mark a session as interrupted, which means that at the
   * appropriate time, we can switch force set the status of
   * the session. You'd only use this if you know what you're doing.
   */
  server.post(v1('/session/interrupt/:id/:status'),
    respond(SessionManager.interruptSession));
}
