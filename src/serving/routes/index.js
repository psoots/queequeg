import IndexManager from '../../api/index/manager';
import {
  respond,
  versioned,
} from '../response';


export default function indexRoutes(server) {
  const v1 = versioned(['0.1.0']);

  /**
   * Crawl through the URLs in the catalog and index them
   */
  server.post(v1('/run/:name'),
    respond((name, body) => {
      if (body) {
        // todo
      }
      return IndexManager.run(name);
    })
  );
}
