import SessionManager from '../../api/session/manager';
import {
  respond,
  versioned,
} from '../response';


export default function statusRoutes(server) {
  const v1 = versioned(['0.1.0']);

  /**
   * Get the status of the catalog
   */
  server.get(v1('/status/:name'),
    respond((name) => SessionManager.getStatus(name)));
}
