import server from '../server';
import log from '../../log';
import indexerStart from '../daemons/indexer';
import migrate from '../init/migrate';

const socketLocation = process.env.NODE_SERVER_SOCKET_PATH;


function startDaemon() {
  indexerStart();

  log.info('Indexing daemon running');
  return Promise.resolve();
}


function startServer() {
// See http://stackoverflow.com/questions/7059518/setup-of-nginx-with-node-js
  const oldUmask = process.umask('0000');
  server.listen(socketLocation, function(err) {
    if (err) {
      log.info(err);
      return;
    }

    log.info('Starting Queequeg...');
    log.info('  Environment is ' + process.env.NODE_ENV);
    log.info('  Listening at ' + socketLocation);

    process.umask(oldUmask);
  });
}


function gracefulShutdown() {
  log.info('Received kill signal, shutting down gracefully.');
  server.close(function() {
    log.info('Closed out remaining connections.');
    process.exit();
  });

  // if after
  setTimeout(function() {
    log.warn('Could not close connections in time, forcefully shutting down');
    process.exit();
  }, 10 * 1000);
}

// listen for TERM signal .e.g. kill
process.on('SIGTERM', gracefulShutdown);

// listen for INT signal e.g. Ctrl-C
process.on('SIGINT', gracefulShutdown);

migrate()
  .then(startDaemon)
  .then(startServer);
