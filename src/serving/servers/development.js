import server from '../server';
import log from '../../log';
import indexerStart from '../daemons/indexer';
import migrate from '../init/migrate';

function startDaemon() {
  indexerStart();

  log.info('Indexing daemon running');
  return Promise.resolve();
}

function startServer() {
  server.listen(8080, () => {
    log.info('Server running on http://localhost:8080');
  });
}

migrate()
  .then(startDaemon)
  .then(startServer);

