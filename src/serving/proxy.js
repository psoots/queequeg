import proxy from 'http-proxy-middleware';
import {noop} from '../tools';

const searchHost = process.env.ELASTICSEARCH_HOST || 'queequeg.cic-stg.com';
const searchPort = process.env.ELASTICSEARCH_PORT || 9200;


export default function searchProxy(server) {
  server.use(proxy('/search', {
    target: `http://${searchHost}:${searchPort}`,
    pathRewrite: {
      '^/search': '',
    },
  }));


  ['get', 'post', 'put', 'patch', 'del', 'head'].forEach((method) =>
    server[method].call(server, /^\/search(.*)/, noop)
  );
}

