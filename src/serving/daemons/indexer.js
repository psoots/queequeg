import IndexManager from '../../api/index/manager';

const interval = 5000;


export default function start() {

  function run() {
    IndexManager.cleanup()
      .then(IndexManager.runAll)
      .then(() => setTimeout(run, interval));
  }

  setTimeout(run, interval);
}
