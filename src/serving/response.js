import vals from 'mout/object/values';

export function badRequest(err, res, next) {
  res.send(400, {error: err, success: false});
  next();
}

export function success(res, next, val = null) {
  const obj = {success: true};
  if (val) {
    obj.result = val;
  }
  res.send(obj);
  next();
}

export function error(err, res, next) {
  const msg = err instanceof Error ? err.message : err;
  res.send(500, {error: msg, success: false});
  next();
}

export function handlePromise(p, res, next) {
  p.then((result) => {
    success(res, next, result);
  }).catch((err) => {
    error(err, res, next);
  });
}

export function handlePromiseWithResult(p, res, next) {
  p.then((result) => {
    success(res, next, result);
  }).catch((err) => {
    error(err, res, next);
  });
}

export function respondWithBody(cb) {
  return (req, res, next) => {
    if (!req.body) {
      return badRequest('No data was posted. Cannot allow request.', res, next);
    }
    handlePromise(cb(...vals(req.params), req.body), res, next);
  };
}

export function respond(cb) {
  return (req, res, next) => {
    handlePromise(cb(...vals(req.params), req.body), res, next);
  };
}

export function versioned(version) {
  return (path) => {
    return {path, version};
  };
}
