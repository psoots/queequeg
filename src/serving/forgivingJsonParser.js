import errors from 'restify-errors';

/**
 * Let's say the user forgets to send the JSON content-type header
 * with a request. Well, we end up using the body as a string instead
 * of a parsed object. We _could_ throw an error and demand that
 * the correct content-type header be passed. _Or_ we could just try
 * to parse the damn thing and then move on like normal.
 *
 * @param req
 * @param res
 * @param next
 */
export default function forgivingJsonBodyParser(req, res, next) {
  if (req.body && req.getContentType() !== 'application/json') {
    let params;
    try {
      params = JSON.parse(req.body);
    } catch (e) {
      next(new errors.InvalidContentError('Invalid JSON: ' + e.message));
      return;
    }

    req._body = req.body;
    req.body = params;
  }
  next();
}
