import omit from '../../../node_modules/mout/object/omit';
import merge from '../../../node_modules/mout/object/merge';
import size from '../../../node_modules/mout/object/size';
import isArray from '../../../node_modules/mout/lang/isArray';
import isString from '../../../node_modules/mout/lang/isString';
import forEach from '../../../node_modules/mout/collection/forEach';
import forOwn from '../../../node_modules/mout/object/forOwn';
import {hash} from '../../tools';


const specialKeys = [
  'url',
  'id',
  'selectors',
  'properties',
  'error',
];

/**
 * Always returns an array of URL objects
 *
 * @param {Array, Object} src
 * @param {Object} other
 * @returns {Array}
 */
export function normalizeForUrls(src, other = {}) {
  const out = [];
  const base = {};
  const props = omit(other, specialKeys);
  if (other.selectors) {
    base.selectors = other.selectors;
  }
  if (size(props) > 0) {
    base.properties = props;
  }

  forEach(src, (urlSrc) => {
    if (isString(urlSrc)) {
      out.push(merge(base, {url: urlSrc, id: hash(urlSrc)}));
      return;
    }

    if (urlSrc.url) {
      urlSrc.id = hash(urlSrc.url);
      out.push(merge(base, urlSrc));
    }
  });
  return out;
}

/**
 * @param {Object} src
 * @param {String} type
 * @param {Object} settings
 * @returns {Object}
 */
export function normalizeForType(src, type, settings = {}) {
  const out = {};


  if (isArray(src)) {
    out[type] = normalizeForUrls(src, settings);
    return out;
  } else {
    out[type] = [];
  }

  if (src.urls) {
    out[type] = normalizeForUrls(src.urls, merge(settings, omit(src, 'urls')));
  }

  return out;
}

export function normalizeTypes(src) {
  let out = {};
  const types = src.types ? src.types : src;
  const settings = src.types ? omit(src, 'types') : {};


  forOwn(types, (typeSrc, typeName) => {
    out = merge(out, normalizeForType(typeSrc, typeName, settings));
  });

  return out;
}


/**
 * Normalizes the requests body for catalogs.
 *
 * At the top level, everything is a "type" and each type has
 * an array of URL objects.
 * {
 *   "type1": [
 *     {url: '...'},
 *     {url: '...'},
 *   ],
 *   "type2": [...]
 * }
 *
 * However, the resulting URL objects can have some extra information that
 * can be inherited from above.
 *
 * So to specify extra fields at the top level, you'd do something like:
 * {
 *   "other": {...},
 *   "types": {
 *     "type1": [...],
 *     "type2": [...],
 *   }
 * }
 *
 * And to specify info only at the type level, you'd do:
 * {
 *   "type1": {
 *     "other": {...},
 *     "urls": [...],
 *   }
 * }
 *
 * Basically, at the top level the key "types" has special meaning and
 * at the type level the key "urls" has special meaning. However, after
 * using this function, the resulting object will have a normalized structure.
 *
 * @param {String} type
 */
export default function normalizer(type = null) {
  if (type) {
    return (src) => {
      return normalizeForType(src, type);
    };
  }
  return normalizeTypes;
}
