import knex from '../../database/connection';
import normalizer from './normalizer';
import forOwn from 'mout/object/forOwn';
import forEach from 'mout/array/forEach';
import Instruction from '../../database/records/instruction';
import Session from '../../database/records/session';
import {decide, head, spread, daysAgo, collectPromises} from '../../tools';
import chunk from 'lodash.chunk';
import {formatDate} from '../../database/records/base';

const statuses = [
  'started',
  'pending',
  'paused',
  'finished',
];
const interruptableStatuses = [
  'paused',
  'finished',
];

const normalize = normalizer();

function allowedStatus(stat) {
  return statuses.indexOf(stat) !== -1;
}

function allowedInterruptStatus(stat) {
  return interruptableStatuses.indexOf(stat) !== -1;
}

function loopThruRaw(raw, cb) {
  const data = normalize(raw);
  const results = [];
  forOwn(data, (srcs, type) => {
    forEach(srcs, (src) => {
      results.push(cb(src, type));
    });
  });
  return results;
}

function isStarted(trx, index) {
  return trx('sessions')
    .where('status', 'started')
    .andWhere({index})
    .count('id as count')
    .first('count')
    .then((row) => row.count > 0);
}

function firstPending(trx, index) {
  return trx('sessions')
    .where('status', 'pending')
    .andWhere({index})
    .orderBy('created_at', 'asc')
    .limit(1)
    .first('id');
}

function firstPaused(trx, index) {
  return trx('sessions')
    .where('status', 'paused')
    .andWhere({index})
    .orderBy('created_at', 'asc')
    .limit(1)
    .first('id');
}

function createSession(trx, obj = {}) {
  return trx.insert(Session.make(obj), 'id')
    .into('sessions')
    .then(head);
}

function setSessionStatus(id, status, trx = knex) {
  return trx('sessions')
    .update('status', status)
    .where({id});
}

const giveNull = () => null;

function makeInstructions(method, sessionId, index, data) {
  return loopThruRaw(data, (src, type) => {
    return Instruction.make({
      method: method,
      url: src.url,
      config: JSON.stringify(src),
      type,
      index,
      hash: src.id,
      session_id: sessionId,
    });
  });
}

function insertInstructions(method, index, data) {
  return knex.transaction((trx) => {
    return createSession(trx, {index})
      .then((sessionId) => {
        const instructions = makeInstructions(method, sessionId, index, data);
        const promises = [];
        forEach(chunk(instructions, 20), (batch) => {
          promises.push(trx('instructions').insert(batch));
        });
        return Promise.all(promises).then(() => sessionId);
      });
  });
}

function errorsBySession(sessionId, trx = knex) {
  return trx('errors').where('session_id', sessionId);
}

function instructionsCountBySession(sessionId, trx = knex) {
  return trx('instructions')
    .where('session_id', sessionId)
    .count('id as count')
    .first('count')
    .then((row) => row.count);
}

function addSessionDetails(sessionRow, trx = knex) {
  return new Promise((ready) => {
    Promise.all([
      errorsBySession(sessionRow.id, trx),
      instructionsCountBySession(sessionRow.id, trx),
    ]).then(spread((errors, instructionsRemaining) => {
      ready(Object.assign({}, sessionRow, {errors, instructionsRemaining}));
    }));
  });
}

function sessionsToStatus(sessions, trx = knex) {
  if (!sessions || sessions.length === 0) {
    return Promise.resolve({
      status: 'waiting',
      sessions: {
        started: [],
        paused: [],
        pending: [],
        finished: [],
      },
    });
  }

  const out = {status: 'pending'};
  const promises = [];

  forEach(sessions, (s) => {
    promises.push(addSessionDetails(s, trx));
  });

  return Promise.all(promises)
    .then((resolvedSessions) => {
      const resolved = resolvedSessions ? resolvedSessions : [];
      out.sessions = {
        started: resolved.filter((x) => x.status === 'started'),
        paused: resolved.filter((x) => x.status === 'paused'),
        pending: resolved.filter((x) => x.status === 'pending'),
        finished: resolved.filter((x) => x.status === 'finished'),
      };
      if (resolvedSessions.length === 0) {
        out.status = 'waiting';
      } else if (out.sessions.started.length > 0) {
        out.status = 'started';
      } else if (out.sessions.paused.length > 0) {
        out.status = 'paused';
      }

      return out;
    });
}

export default class SessionManager {

  static addAll(index, data) {
    return insertInstructions('add', index, data);
  }

  static deleteAll(index, data) {
    return insertInstructions('delete', index, data);
  }

  // status is an aggregate thing
  static getStatus(index) {
    return knex.transaction((trx) => {
      return trx('sessions')
        .where({index})
        .andWhere(function() {
          // for finished sessions, we only need the most recent
          this.whereNot({status: 'finished'})
            .orWhere('created_at', '>', formatDate(daysAgo(3)));
        })
        .orderBy('created_at', 'asc')
        .then((sessions) => sessionsToStatus(sessions, trx));
    });
  }

  static checkoutOldest(name) {
    return knex.transaction((trx) => {
      const startAndReturn = (row) => {
        if (!row) return null;
        return SessionManager.startSession(row.id, trx)
          .then(() => SessionManager.sessionById(row.id, trx));
      };

      const checkout = () => {
        // if there's a paused session, use that first
        return firstPaused(trx, name)
          .then((row) => {
            if (!row) return firstPending(trx, name).then(startAndReturn);
            return startAndReturn(row);
          });
      };

      return isStarted(trx, name)
        .then(decide(giveNull, checkout));
    });
  }

  static interruptSession(id, newStatus, trx = knex) {
    if (!allowedInterruptStatus(newStatus)) return Promise.resolve();
    return trx('sessions')
      .update({interrupt: newStatus})
      .where({id});
  }

  static applyInterrupts(trx = knex) {
    return trx('sessions')
      .whereIn('interrupt', interruptableStatuses)
      .then((rows) => collectPromises(rows, (row) =>
        trx('sessions')
          .update({
            status: row.interrupt,
            interrupt: null,
            started_at: null,
            updated_at: formatDate(new Date),
          })
          .where({id: row.id})
        )
      );
  }

  static sessionById(id, trx = knex) {
    return trx('sessions').where('id', id).first('*');
  }

  static pauseSession(id, trx = knex) {
    return trx('sessions')
      .update({
        status: 'paused',
        started_at: null,
        updated_at: formatDate(new Date),
      })
      .where({id});
  }

  static finishSession(id, trx = knex) {
    return trx('sessions')
      .update({
        status: 'finished',
        started_at: null,
        updated_at: formatDate(new Date),
      })
      .where({id});
  }

  static startSession(id, trx = knex) {
    const stamp = formatDate(new Date);
    return trx('sessions')
      .update({
        status: 'started',
        started_at: stamp,
        updated_at: stamp,
      })
      .where({id});
  }
}
