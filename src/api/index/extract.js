import cheerio from 'cheerio';
import applyNestedName from './nested';
import isString from 'mout/lang/isString';
import strip from 'striptags';

function stripHtml(html) {
  const res = strip(html);
  if (!isString(res)) return res;
  return res.replace(/\s{2,}/g, ' ');
}

export const defaults = {

  // which elements in the DOM should be added as fields on the ES Document
  // the value of this attribute is the field's name
  //
  // REQUIRED
  //
  // i.e. <li data-qq="birds[]">Canary</li>
  field: '[data-qq]',
  fieldAttr: 'data-qq',

  // if provided, use the value assigned to this attribute.
  // if not provided, use the element's text node
  // i.e. <li data-qq="birds[]" data-qq-value="Canary"><em>Canary</em></li>
  fieldValueAttr: 'data-qq-value',

  // if provided, allows for some specified processing actions
  //
  // the available options are:
  // "raw" - grab straight html
  //
  // if not provided, only the text will be extracted
  // with or without processing, all values are trimmed of whitespace
  processAttr: 'data-qq-process',

  // if provided, set the field type on the ES Document (for analysis)
  // using dynamic mapping instead:
  // https://www.elastic.co/guide/en/elasticsearch/reference/current/dynamic-field-mapping.html
  // fieldType: '[data-qq-type]',

  // store the html within this element as the ES Document's "content" field.
  // the "content" field gets special treatment
  content: '[data-qq-content]',


  // which elements should be removed from the "content" field
  ignore: '[data-qq-ignore]',

  // which tags in the "content" field should become "tag_***" fields on the ES Document
  // these fields can offer a chance to add weights when searching
  // i.e. if "h1" tag is provided here, then all <h1> values will be concatenated into the field "tags_h1"
  tags: [
    'h1', 'h2', 'h3', 'h4', 'h5', 'h6',
    'title',
  ],

  // any default properties that will be applied to the document.
  // they can be overridden by anything found in the html.
  properties: {},
};

function process(actionStr, rawValue) {
  const actions = actionStr.split(/\s+/);
  if (actions.length === 0) return rawValue;
  let result = rawValue;

  actions.forEach((action) => {
    switch (action) {
    case 'raw':
    default:
      result = rawValue;
      break;
    }
  });

  if (!result || !isString(result)) {
    return null;
  }

  return result.trim();
}


function valuator($, attrSelector, attrProcess) {
  // must be a real function, we are using "this"
  return function() {
    // allows overriding the value with an attribute
    // instead of pulling it from the element body
    const override = $(this).attr(attrSelector);
    const applyOverride = (cb) => override === undefined ? cb() : override;

    const actions = $(this).attr(attrProcess);
    const hasActions = actions !== undefined;
    const actionStr = hasActions ? actions : '';

    // if processing instructions were provided, then use HTML otherwise use TEXT
    const rawValue = applyOverride(() => hasActions ? $(this).html() : $(this).text());

    return process(actionStr, rawValue);
  };
}

export default function extract(html, options = {}) {
  const opts = Object.assign({}, defaults, options);
  const $ = cheerio.load(html);
  const getValue = valuator($, opts.fieldValueAttr, opts.processAttr);

  let result = Object.assign({}, opts.properties);

  $(opts.field).each((i, el) => {
    const name = $(el).attr(opts.fieldAttr);
    const val = getValue.call(el);
    result = applyNestedName(result, name, val);
  });

  $(opts.ignore).remove();

  const $content = $(opts.content);

  result.content = stripHtml($content.html());

  if (opts.tags.length) {
    result.tags = {};
    opts.tags.forEach((tag) => {
      result.tags[tag] = $content
        .find(tag)
        .map(getValue)
        .get();
    });
  }
  return result;
}
