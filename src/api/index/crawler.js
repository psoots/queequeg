import SimpleCrawler from 'simplecrawler';
import forEach from 'mout/collection/forEach';
import forOwn from 'mout/object/forOwn';
import map from 'mout/array/map';
import groupBy from 'mout/array/groupBy';
import iconv from 'iconv-lite';
import URI from 'urijs';


// We can only create one crawler instance per host.
function groupKeyer(raw) {
  const uri = new URI(raw);
  return uri.scheme() + '://' + uri.hostname() + ':' + uri.port();
}

function resourceFromUrl(raw) {
  return (new URI(raw)).resource();
}

function ignoreUnlisted(listedUrls) {
  return (parsedUrl) => {
    return listedUrls.indexOf(resourceFromUrl(URI.build(parsedUrl))) !== -1;
  };
}


function createCrawler(baseUrl, urls) {
  const uri = new URI(baseUrl);
  const urlData = {
    hostname: uri.hostname(),
    protocol: uri.protocol(),
    port: uri.port(),
  };

  const crawler = new SimpleCrawler(urlData.hostname);

  if (urlData.port) {
    crawler.initialPort = parseInt(urlData.port, 10);
  } else if (urlData.protocol === 'https') {
    crawler.initialPort = 443;
  }
  crawler.initialProtocol = urlData.protocol;


  // Since we're supplying the to-be-crawled URLs, we need to
  // prevent typical crawling behavior
  crawler.discoverResources(() => {});
  crawler.addFetchCondition(ignoreUnlisted(urls));
  crawler.maxDepth = 2;

  crawler.decodeResponses = true;

  return crawler;
}


function runCrawler(crawler, urls, retrievedCB, errorCB) {
  return new Promise((ready) => {
    // If the callback returns a promise, then pause the
    // crawler until the callback's promise returns.
    const hold = (res) => {
      if (res instanceof Promise) {
        return res.then(crawler.wait());
      }
    };

    const fetchError = (msg) => (queueItem, ...args) => {
      return hold(errorCB(queueItem.url, new Error(msg), args));
    };

    // done with our batch, carry on
    crawler.on('complete', ready);

    // Finished a single URL
    crawler.on('fetchcomplete', (queueItem, responseBody) => {
      const body = responseBody instanceof Buffer ? iconv.decode(responseBody, 'utf-8') : responseBody;
      return hold(retrievedCB(queueItem.url, body));
    });

    crawler.on('queueerror', (errorData, parsedUrl) => {
      const url = URI.build(parsedUrl);
      return hold(errorCB(url, new Error(`Issues with adding URL to the crawler queue: ${url}`), errorData));
    });

    crawler.on('fetchdataerror', fetchError('Response too big.'));
    crawler.on('fetchredirect', fetchError('Redirect response'));
    crawler.on('fetch404', fetchError('404 not found'));
    crawler.on('fetcherror', fetchError('400/500 series error'));
    crawler.on('fetchtimeout', fetchError('Timeout'));
    crawler.on('fetchclienterror', fetchError('Queequeg error, please forgive and report'));

    // Add _all_ URLs to the queue
    forEach(urls, crawler.queueURL.bind(crawler));

    crawler.start();
  });
}


/**
 * Crawls a list of URLs
 *
 * @param {Array,Iterator.<V>} urls
 * @param {Function} retrievedCB
 * @param {Function} errorCB
 * @returns {Promise}
 */
export default function crawl(urls, retrievedCB, errorCB) {
  const promises = [];
  const grouped = groupBy(urls, groupKeyer);
  forOwn(grouped, (ourUrls, common) => {
    const resourceUrls = map(ourUrls, resourceFromUrl);
    const crawler = createCrawler(common, resourceUrls);
    const p = runCrawler(crawler, resourceUrls, retrievedCB, errorCB);
    promises.push(p);
  });

  return Promise.all(promises);
}


