import SessionManager from '../session/manager';
import knex from '../../database/connection';
import SearchManager from '../search/manager';
import Instruction from '../../database/records/instruction';
import extract from './extract';
import crawl from './crawler';
import forEach from 'mout/array/forEach';
import groupBy from 'mout/array/groupBy';
import log from '../../../src/log';
import pluck from 'mout/array/pluck';
import omit from 'mout/object/omit';
import URI from 'urijs';
import {curry, spread, minutesAgo, collectPromises} from '../../tools';
import {formatDate} from '../../database/records/base';


function urlToPath(url) {
  return URI.build(omit(URI.parse(url), 'username', 'password', 'protocol', 'hostname', 'port'));
}

function splitInstructions(instructions) {
  const grouped = groupBy(instructions, (instr) => instr.method);
  return [grouped['add'], grouped['delete']];
}

function findInstructionsForSession(sessionId, batchSize) {
  return knex('instructions')
    .where('session_id', sessionId)
    .limit(batchSize);
}

function countInstructionsRemaining(sessionId) {
  return knex('instructions')
    .where('session_id', sessionId)
    .count('id as count')
    .first('count')
    .then((row) => row.count);
}

function instructionFromMap(map, url) {
  let instr = map.get(url);
  if (!instr) {
    // if we were given a relative url, then we won't find the instruction
    // we're looking for with the given url because the crawler prepends
    // the host & port. So, let's just try to find the instruction using the path only.
    instr = map.get(urlToPath(url));
  }

  if (!instr) {
    log.error(`Could not find instruction for the url: '${url}'`);
    // todo better error handling
    return Promise.reject();
  }
  return Promise.resolve(instr);
}

// The instruction's config holds the stuff we need to do the extraction:
// "properties" <== a starting point of properties to add to the extracted document
// "selectors"  <== any overrides of the default selectors used to extract properties
function doExtraction(instr, html) {
  const conf = JSON.parse(instr.config);
  const opts = Object.assign({properties: conf.properties}, conf.selectors);
  const extracted = extract(html, opts);

  // it's impossible to re-write the url from within document
  return Object.assign({}, extracted, {url: instr.url});
}

function handleIndexing(map, url, html) {
  log.info('indexing...', url);
  return instructionFromMap(map, url)
    .then((instr) => Promise.all([
      SearchManager.saveDocument(instr.index, instr.type, instr.url, doExtraction(instr, html)),
      knex('instructions').where('id', instr.id).del(),
    ]));
}

function handleError(map, url, error) {
  log.error('error...', url, error);
  return instructionFromMap(map, url)
    .then((instr) => {
      const err = (new Instruction(instr))
        .convertToError(error)
        .toObject();

      return Promise.all([
        knex('errors').insert(err),
        knex('instructions').where('id', instr.id).del(),
      ]);
    });
}

function runAddInstructions(instructions) {
  if (!instructions || instructions.length === 0) {
    return Promise.resolve();
  }
  const map = new Map();
  const urls = pluck(instructions, 'url');
  forEach(instructions, (instr) => map.set(instr.url, instr));

  const doIndex = curry(handleIndexing, map);
  const doError = curry(handleError, map);

  return crawl(urls, doIndex, doError);
}

function runDeleteInstructions(instructions) {
  if (!instructions || instructions.length === 0) {
    return Promise.resolve();
  }

  return collectPromises(instructions, (instr) => {
    log.info('deleting...', instr.url);
    return SearchManager.removeDocument(instr.index, instr.type, instr.url)
      .then(() => knex('instructions').where('id', instr.id).del());
  });
}

export default class IndexManager {

  static run(index, batchSize = 10) {
    return SessionManager.checkoutOldest(index)
      .then((session) => {
        if (!session) {
          return Promise.resolve('busy');
        }
        return findInstructionsForSession(session.id, batchSize)
          .then(splitInstructions)
          .then(spread((toAdd, toDelete) => Promise.all([
            runAddInstructions(toAdd),
            runDeleteInstructions(toDelete),
          ])))
          .then(() => countInstructionsRemaining(session.id))
          .then((count) => {
            // todo maybe do a little more here, like tally errors,
            // see if anything is amiss.

            if (count > 0) {
              return SessionManager.pauseSession(session.id)
                .then(() => 'paused');
            }

            return SessionManager.finishSession(session.id)
              .then(() => 'finished');
          });
      });
  }


  static runAll() {
    return knex('sessions')
      .distinct('index')
      .select('index')
      .then((rows) => Promise.all(
        rows.map((row) => IndexManager.run(row.index))
      ));
  }

  static cleanup() {
    // if a session gets stuck with a "started" status, but no one is
    // actually working on that session, then no sessions would ever get
    // started ever again. So, we need to reset sessions that are started and
    // not completed by a certain time. We just change the status back to "paused".
    // Nothing complicated.

    // we can also apply interrupts at this point too
    // which allows admin to force-set session statuses.


    const updateSessions = (trx) => {
      return trx('sessions')
        .update({
          status: 'paused',
          started_at: null,
          updated_at: formatDate(new Date),
        })
        .where('status', 'started')
        .andWhere('started_at', '<', formatDate(minutesAgo(5)));
    };

    return knex.transaction((trx) =>
      SessionManager.applyInterrupts(trx)
        .then(() => updateSessions(trx))
    );
  }
}
