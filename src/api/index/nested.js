import isArray from 'mout/lang/isArray';
import isObject from 'mout/lang/isObject';
import isNumber from 'mout/lang/isNumber';
import forOwn from 'mout/object/forOwn';
import every from 'mout/object/every';
import objectMap from 'mout/object/map';
import values from 'mout/object/values';

// more than merges objects, this function will also
// concat arrays from the source to the target
// NOTE: we only concat if the target prop already has an array
function combineObjects(target, source) {
  const newTarget = Object.assign({}, target);
  forOwn(source, (sourceVal, prop) => {
    const targetVal = newTarget[prop];
    if (targetVal) {
      if (isArray(targetVal)) {
        newTarget[prop] = targetVal.concat(sourceVal);
        return;
      }
      if (isObject(targetVal)) {
        newTarget[prop] = combineObjects(targetVal, sourceVal);
        return;
      }
    }

    newTarget[prop] = sourceVal;
  });

  return newTarget;
}


/**
 * Takes a name representing a nested array value, like:
 *
 * "some[deep][][structured][things][]"
 *
 * And puts the given value in the right place on the given object:
 *
 * {
 *   some: {
 *     deep: [
 *       {
 *         structured: {
 *           things: [...value...]
 *         }
 *       }
 *     ]
 *   }
 * }
 *
 * @param {Object} obj
 * @param {String} name
 * @param {*} value
 * @returns {Object}
 */
export default function applyNestedName(obj, name, value) {
  // matches birds[][breeds][] => "birds[][","birds","[][" and "breeds][]","breeds","][]"
  const re = /([^\]\[]+)([\]\[]+)*/g;
  const reIsArrayBraks = /\[]/;
  const newObj = Object.assign({}, obj);

  // make a map of property names and corresponding brackets
  // the brackets determine whether the next value is an object or an array value
  const fields = [];
  let parts = re.exec(name);
  while (parts !== null) {
    fields.push([parts[1], parts[2]]);
    parts = re.exec(name);
  }

  // reverse the array to build from the leaf up
  const map = new Map(fields.reverse());

  let lastVal = value;
  for (const [prop, braks] of map.entries()) {
    if (braks && reIsArrayBraks.test(braks)) {
      lastVal = {[prop]: [lastVal]};
    } else {
      lastVal = {[prop]: lastVal};
    }
  }

  return combineObjects(newObj, lastVal);
}
