import ElasticSearch from 'elasticsearch';

const searchHost = process.env.ELASTICSEARCH_HOST;
const searchPort = process.env.ELASTICSEARCH_PORT;
const apiVersion = process.env.ELASTICSEARCH_VERSION;

export default new ElasticSearch.Client({
  host: `${searchHost}:${searchPort}`,
  log: [
    'error',
    // 'trace',
    // 'debug',
    'warning',
  ],
  apiVersion,
});
