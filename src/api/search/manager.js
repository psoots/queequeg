import ops from './operations';
import {hash} from '../../tools';

export default class SearchManager {
  static saveDocument(index, type, url, body) {
    const id = hash(url);
    return ops.createIndex(index)
      .then(() => ops.insert(index, type, id, body));
  }

  static removeDocument(index, type, url) {
    return ops.deleteDoc(index, type, hash(url));
  }
}
