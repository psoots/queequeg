import client from './connnection';
import map from 'mout/collection/map';
import values from 'mout/object/values';
import objMap from 'mout/object/map';
import isArray from 'mout/lang/isArray';
import forEach from 'mout/array/forEach';
import merge from 'mout/object/merge';
import keys from 'mout/object/keys';
import {noop, decide} from '../../tools';

const defaultIndexMappings = {
  _default_: {
    _timestamp: {
      enabled: true,
    },
  },
};

function mapAndCollectPromises(method, collection, mapMethod) {
  let promises = mapMethod(collection, (v, k) => {
    return method.apply(this, [v, k]);
  });

  if (!isArray(promises)) {
    promises = values(promises);
  }

  return Promise.all(promises);
}

function doScroll(response, cb, hitsCount = 0, resultPromises = []) {
  return client.scroll({
    scrollId: response._scroll_id,
    scroll: '1m',
  }).then(scroller(cb, hitsCount, resultPromises));
}

function scroller(cb, hitsCount = 0, resultPromises = []) {
  let ourHitsCount = hitsCount;
  return (response) => {
    ourHitsCount += response.hits.hits.length;

    resultPromises.push(cb(response));

    if (response.hits.total === ourHitsCount) return Promise.all(resultPromises);

    return doScroll(response, cb, ourHitsCount, resultPromises);
  };
}

/**
 * Low level API operations
 */
export default class SearchOperations {

  static aliasExists(alias) {
    return client.indices.existsAlias({index: '*', name: alias});
  }

  static indexExists(index) {
    return client.indices.exists({index});
  }

  static updateAlias(alias, oldIndex, newIndex) {
    return client.indices.updateAliases({
      body: {
        actions: [
          {remove: {index: oldIndex, alias}},
          {add: {index: newIndex, alias}},
        ],
      },
    });
  }

  static deleteIndex(index) {
    return client.indices.delete({index, ignore: 404});
  }


  static createIndex(index) {
    const doCreate = () => {
      return client.indices.create({
        index,
        body: {mappings: defaultIndexMappings},
      });
    };
    return client.indices.exists({index})
      .then(decide(noop, doCreate));
  }

  static getAlias(name) {
    return client.indices.getAlias({index: '*', name});
  }

  static createAlias(name, index) {
    return client.indices.putAlias({name, index});
  }

  static deleteAlias(name) {
    return client.indices.deleteAlias({index: '*', name, ignore: 404});
  }

  static bulk(body) {
    return client.bulk({body});
  }

  static count(index, query = null) {
    return client.indices.refresh({index})
      .then(() => {
        if (query) {
          return client.count({index, body: {query}});
        }
        return client.count({index});
      });
  }

  static refresh(index) {
    return client.indices.refresh({index});
  }

  static getDocument(index, type, id) {
    return client.get({index, type, id});
  }

  static updateDocument(index, type, id, body) {
    return client.update({index, type, id, body: {doc: body}});
  }

  static documentExists(index, type, id) {
    return client.exists({index, type, id});
  }

  static deleteDoc(index, type, id) {
    return client.delete({index, type, id, ignore: 404});
  }

  static getTypes(index) {
    return client.indices.getMapping({index})
      .then((info) => {
        if (!info[index] || !info[index].mappings) {
          return [];
        }
        return keys(info[index].mappings).filter((type) => ! /^_/.test(type));
      });
  }

  static insert(index, type, id, body) {
    return client.index({index, type, id, body});
  }

  static scanAll(index, queryObj, size, cb) {
    const opts = merge({
      index,
      size,
      scroll: '1m',
      search_type: 'scan',
    }, queryObj);

    // gotta refresh! or we could lose some stuff
    // use something else if performance is an issue
    return SearchOperations.refresh(opts.index)
      .then(() => {
        return client.search(opts);
      })
      .then((response) => {
        if (!response.hits.total) return;
        return doScroll(response, cb);
      });
  }

  static scanAllHits(index, queryObject, size, cb) {
    return SearchOperations.scanAll(index, queryObject, size, (response) => {
      forEach(response.hits.hits, cb);
    });
  }

  static copy(fromIndex, toIndex, queryObj = {}, size = 10) {
    return SearchOperations.scanAll(fromIndex, queryObj, size, (response) => {
      const bulkOps = [];

      forEach(response.hits.hits, (hit) => {
        bulkOps.push({index: {_index: toIndex, _type: hit._type, _id: hit._id}});
        bulkOps.push(hit._source);
      });

      return SearchOperations.bulk(bulkOps);
    });
  }

  static deleteIndices(indices) {
    return mapAndCollectPromises(SearchOperations.deleteIndex, indices, map);
  }
  static createAliases(aliases) {
    return mapAndCollectPromises(SearchOperations.createAlias, aliases, objMap);
  }
  static deleteAliases(aliases) {
    return mapAndCollectPromises(SearchOperations.deleteAlias, aliases, map);
  }
}

