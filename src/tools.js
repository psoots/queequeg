import fillIn from 'mout/object/fillIn';
import pick from 'mout/object/pick';
import keys from 'mout/object/keys';
import crypto from 'crypto';


export function collectPromises(items, cb) {
  const promises = [];
  for (const item of items) {
    promises.push(cb(item));
  }
  return Promise.all(promises);
}

export function makePromiseCollector(cb) {
  return (items) => {
    return collectPromises(items, cb);
  };
}

// merges in defaults while omitting anything but the keys provided by the defaults
export function applyStrictDefaults(obj, defaults) {
  return pick(fillIn(obj, defaults), keys(defaults));
}

export function decide(ifTrueCallback, ifFalseCallback, ...args) {
  return (bool) => (bool ? ifTrueCallback : ifFalseCallback).apply(this, args);
}

export function hash(str) {
  if (!str) return false;
  return crypto.createHash('md5').update(str).digest('hex');
}


export function noop() {}

export function ifTrue(ifTrueCallback, ...args) {
  return decide(ifTrueCallback, noop, ...args);
}

export function head(arr) {
  return arr[0] || null;
}

export function daysAgo(days = 1) {
  const d = new Date;
  return new Date(d.getFullYear(), d.getMonth(), d.getDate() - days, 0, 0, 0);
}

export function minutesAgo(minutes = 1) {
  const d = new Date;
  return new Date(d.getFullYear(), d.getMonth(), d.getDate(), d.getHours(), d.getMinutes() - minutes, 0);
}

/**
 * Creates a function that will be passed the
 * provided arguments at the time of calling.
 *
 * @param {Function} func
 * @param args1
 * @returns {Function}
 */
export function curry(func, ...args1) {
  return (...args2) => func.call(this, ...args1, ...args2);
}

/**
 * Creates a function that takes an array and applies
 * those values as arguments to the given function.
 *
 * @param {Function} cb
 * @returns {Function}
 */
export function spread(cb) {
  return (arr) => cb.apply(this, arr);
}
