import Bunyan, {ERROR, FATAL, INFO, DEBUG, WARN, TRACE} from 'bunyan';
import pathUtility from 'path';
import map from 'mout/array/map';

const env = process.env.NODE_ENV || 'production';

function fStream(path, level) {
  return {path, level};
}

function prodLogger() {
  const logFile = process.env.LOGFILE_PROD || '/home/queequeg/queequeg.log';
  return Bunyan.createLogger({
    name: 'queequeg',
    streams: [
      {
        stream: process.stdout,
        level: INFO,
      },
      fStream(logFile, INFO),
    ],
  });
}

function devLogFile(path) {
  return pathUtility.resolve(`${__dirname}/../${path}`);
}

function devLogger() {
  return Bunyan.createLogger({
    name: 'queequeg',
    streams: [fStream(devLogFile('queequeg.log'), DEBUG)],
  });
}

export default env === 'development' || env === 'testing' ? devLogger() : prodLogger();
