function twoDigs(x) {
  return x < 10 ? `0${x}` : x;
}
function fourDigs(x) {
  if (x < 10 ) return `000${x}`;
  if (x < 100) return `00${x}`;
  if (x < 1000) return `0${x}`;
  return x;
}

export function formatDate(date) {
  const y = date.getFullYear();
  const m = twoDigs(date.getMonth() + 1);
  const d = twoDigs(date.getDate());
  const th = twoDigs(date.getHours());
  const tm = twoDigs(date.getMinutes());
  const ts = twoDigs(date.getSeconds());
  const tmm = fourDigs(date.getMilliseconds());
  return `${y}-${m}-${d} ${th}:${tm}:${ts}.${tmm}`;
}

export default class BaseRecord {
  constructor(obj = {}) {
    const now = formatDate(new Date);
    this.raw = Object.assign({}, {
      created_at: now,
      updated_at: now,
    }, obj);
  }

  set createdAt(date) {
    this.raw.created_at = formatDate(date);
  }

  set updatedAt(date) {
    this.raw.updated_at = formatDate(date);
  }

  toObject() {
    return Object.assign({}, this.raw);
  }
}
