import BaseRecord from './base';

export default class ErrorRecord extends BaseRecord {
  static make(data = {}) {
    return new ErrorRecord(data).toObject();
  }
}
