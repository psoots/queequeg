import BaseRecord from './base';

export default class SessionRecord extends BaseRecord {
  static make(data = {}) {
    return new SessionRecord(data).toObject();
  }
}
