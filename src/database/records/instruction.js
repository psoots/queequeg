import BaseRecord from './base';
import ErrorRecord from './error';
import pick from 'mout/object/pick';

export default class InstructionRecord extends BaseRecord {
  static make(data = {}) {
    return new InstructionRecord(data).toObject();
  }

  convertToError(err) {
    const toCopy = ['session_id', 'url', 'type', 'index', 'hash', 'config'];
    const info = pick(this.raw, toCopy);
    info.error = err.message;
    return new ErrorRecord(info);
  }
}
