import knexConfig from '../../knexfile';
import knex from 'knex';
import {ifTrue} from '../tools';

const env = process.env.NODE_ENV || 'production';
let dbInstance = null;
if (knexConfig[env]) {
  dbInstance = knex(knexConfig[env]);
}

// fixme
// function doClose() {
//   if (dbInstance) {
//     dbInstance.destroy();
//   }
// }
//
// process.on('SIGTERM', doClose);
// process.on('SIGINT', doClose);

export default dbInstance;


function truncateIfExists(tb) {
  return dbInstance.schema.hasTable(tb)
    .then(ifTrue(() => dbInstance.truncate(tb)));
}

export function clearAllTables() {
  return Promise.all([
    truncateIfExists('instructions'),
    truncateIfExists('errors'),
  ])
    .then(() => truncateIfExists('sessions'))
    .then(() => dbInstance.migrate.latest());
}
