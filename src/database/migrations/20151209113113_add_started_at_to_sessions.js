
exports.up = function(knex) {
  return knex.schema.table('sessions', (t) => {
    t.datetime('started_at').nullable();
  });
};

exports.down = function(knex) {
  return knex.schema.table('sessions', (t) => {
    t.dropColumn('started_at');
  });
};
