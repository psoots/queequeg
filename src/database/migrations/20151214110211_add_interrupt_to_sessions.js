
exports.up = function(knex) {
  return knex.schema.table('sessions', (t) => {
    t.string('interrupt').nullable();
  });
};

exports.down = function(knex) {
  return knex.schema.table('sessions', (t) => {
    t.dropColumn('interrupt');
  });
};
