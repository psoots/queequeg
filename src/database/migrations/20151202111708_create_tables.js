function createSessions(knex) {
  return knex.schema.createTable('sessions', (t) => {
    t.increments('id');
    t.string('index');
    t.enu('status', ['pending', 'started', 'paused', 'finished', 'errors']).defaultTo('pending');
    t.timestamps();
  });
}

function createInstructions(knex) {
  return knex.schema.createTable('instructions', (t) => {
    t.increments('id');
    t.integer('session_id').unsigned().references('id').inTable('sessions');
    t.string('method');
    t.string('url');
    t.string('type');
    t.string('index');
    t.string('hash');
    t.text('config');
    t.timestamps();
  });
}

function createErrors(knex) {
  return knex.schema.createTable('errors', (t) => {
    t.increments('id');
    t.integer('session_id').unsigned().references('id').inTable('sessions');
    t.string('error');
    t.string('url');
    t.string('type');
    t.string('index');
    t.string('hash');
    t.text('config');
    t.timestamps();
  });
}

exports.up = function(knex) {
  return createSessions(knex)
    .then(() => Promise.all([
      createInstructions(knex),
      createErrors(knex),
    ]));
};

exports.down = function(knex) {
  return Promise.all([
    knex.schema.dropTable('instructions'),
    knex.schema.dropTable('errors'),
  ])
    .then(() => knex.schema.dropTable('sessions'));
};
