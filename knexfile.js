var path = require('path');
require('./src/load');

function dir(p) {
  return path.resolve(__dirname + '/src/database/' + p);
}

function conf(overrides) {
  return Object.assign({}, {
    client: 'sqlite3',
    connection: {
      filename: process.env.SQLITE || dir('db.dat'),
    },
    migrations: {
      directory: dir('migrations'),
    },
  }, overrides === null ? {} : overrides);
}

const devConf = conf();
const prodConf = conf();
const testConf = conf({
  connection: {
    filename: path.resolve(__dirname + '/test/db.dat'),
  },
 // debug: true,
});

module.exports = {
  development: devConf,
  production: prodConf,
  testing: testConf,
};
