# config valid only for current version of Capistrano
lock '3.4.0'

set :application, 'queequeg'
set :repo_url, 'git@github.com:castiron/queequeg.git'
set :deploy_to, '/home/queequeg/queequeg'
set :scm, :git
set :format, :pretty
set :log_level, :debug

set :linked_files, ['.env', 'src/database/db.dat']

namespace :deploy do

  desc 'Restart application'
	task :restart do
    on roles(:app), in: :sequence, wait: 5 do
	    execute "sudo stop #{fetch(:application)} || true"
      execute "sudo start #{fetch(:application)}"
    end
  end

  desc 'NPM Install'
  task :npm_install do
    on roles(:app) do
      with path: 'node_modules/.bin:$PATH' do
        within release_path do
          execute :npm, :install
        end
      end
    end
  end

  before 'deploy:publishing', :npm_install
  after :published, :restart
end
